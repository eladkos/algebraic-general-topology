\chapter{Bases on filtrators}

In this chapter I consider generalization of filter bases
for the filtrator of funcoids (and more general filtrators).

\fxwarning{Base must be nonempty.}

\begin{defn}
  A set $S$ of binary relations is a \emph{base} of a funcoid $f$ when all elements
  of $S$ are above $f$ and $\forall X \in \up f \exists T \in S : T
  \sqsubseteq X$.
\end{defn}

\begin{prop}
  A set $S$ of binary relations is a base of a funcoid iff it is a base of
  $\bigsqcap^{\mathsf{FCD}} S$.
\end{prop}

\begin{proof}
  ~
  \begin{description}
    \item[$\Leftarrow$] Obvious.
    
    \item[$\Rightarrow$] Let $S$ be a base of funcoid $f$. All elements of $S$
    are above $f$. So $\bigsqcap^{\mathsf{FCD}} S \sqsupseteq f$. We
    have $\forall X \in \up f \exists T \in S : T \sqsubseteq X$ and
    thus $\forall X \in \up \bigsqcap^{\mathsf{FCD}} S \exists T
    \in S : T \sqsubseteq X$. To finish the proof note that all elements of
    $S$ are above $\bigsqcap^{\mathsf{FCD}} S$.
  \end{description}
\end{proof}

\begin{thm}
  If $S$ is a filter base on the set of binary relations then $S$ is a base of
  $\bigsqcap^{\mathsf{FCD}} S$.
\end{thm}

First prove a special case of our theorem to get the idea:

\begin{example}
  Take $S = \setcond{
  \setcond{ (x, y) }{ | x - y | < \varepsilon }}{ \varepsilon > 0 }$ and $K = \setcond{ (x, y) }{
  | x - y | < \exp x }$ where $x$ and $y$ range real
  numbers. Then $K \notin \up \bigsqcap^{\mathsf{FCD}} S$.
\end{example}

\begin{proof}
  Take a nontrivial ultrafilter $x$ on $\mathbb{R}$. We can for simplicity
  assume $x \sqsubseteq \mathbb{Z}$.
  
  $\supfun{\bigsqcap^{\mathsf{FCD}} S} x =
  \bigsqcap^{\mathscr{F}}_{L \in S} \supfun{L} x =
  \bigsqcap^{\mathscr{F}}_{L \in S, X \in \up x} \rsupfun{L} X =
  \bigsqcap^{\mathscr{F}}_{\varepsilon > 0, X \in \up
  x} \bigsqcup_{\alpha \in X}] \alpha - \varepsilon ; \alpha + \varepsilon [$.
  
  $\supfun{K} x = \bigsqcap^{\mathscr{F}}_{X \in \up x} \rsupfun{K} X =
  \bigsqcap^{\mathscr{F}}_{X \in \up x}
  \bigsqcup_{\alpha \in X}] \alpha - \exp \alpha ; \alpha + \exp \alpha [$.
  
  Suppose for the contrary that $\supfun{K} x \sqsupseteq \supfun{
  \bigsqcap^{\mathsf{FCD}} S } x$.
  
  Then
  
  $\bigsqcup_{\alpha \in X}] \alpha - \exp \alpha ; \alpha + \exp \alpha
  [\sqsupseteq \bigsqcap^{\mathscr{F}}_{\varepsilon > 0, X \in \up x}
  \bigsqcup_{\alpha \in X}] \alpha - \varepsilon ; \alpha + \varepsilon [$ for
  every $X \in \up x$
  
  thus by properties of generalized filter bases ($\setcond{ \bigsqcup_{\alpha
  \in X}] \alpha - \varepsilon ; \alpha + \varepsilon [ }{
  \varepsilon > 0 }$ is a filter base and even a chain)
  
  $\bigsqcup_{\alpha \in X}] \alpha - \exp \alpha ; \alpha + \exp \alpha
  [\sqsupseteq \bigsqcap^{\mathscr{F}}_{X \in \up x} \bigsqcup_{\alpha
  \in X}] \alpha - \varepsilon ; \alpha + \varepsilon [$ for some $\varepsilon
  > 0$ and thus
  
  by properties of generalized filter bases ($\setcond{ \bigsqcup_{\alpha \in
  X}] \alpha - \varepsilon ; \alpha + \varepsilon [ }{
  X \in \up x }$ is a filter base) for some $X' \in \up x$
  
  $\bigsqcup_{\alpha \in X}] \alpha - \exp \alpha ; \alpha + \exp \alpha
  [\sqsupseteq \bigsqcup_{\alpha \in X'}] \alpha - \varepsilon ; \alpha +
  \varepsilon [$
  
  what is impossible by the fact that $\exp \alpha$ goes infinitely small as
  $\alpha \rightarrow - \infty$ and the fact that we can take $X =\mathbb{Z}$
  for some $x$.
\end{proof}

Now prove the general case:

\begin{proof}
  Take an ultrafilter $x$.
  
  $\supfun{\bigsqcap^{\mathsf{FCD}} S} x =
  \bigsqcap^{\mathscr{F}}_{L \in S} \supfun{L} x =
  \bigsqcap^{\mathscr{F}}_{L \in S, X \in \up x} \rsupfun{L} X$.
  
  $\supfun{K} x = \bigsqcap^{\mathscr{F}}_{X \in \up x} \rsupfun{K}X$.
  
  Suppose that $K \in \up \bigsqcap^{\mathsf{FCD}} S$ and thus
  $\supfun{K} x \sqsupseteq \supfun{
  \bigsqcap^{\mathsf{FCD}} S } x$.
  
  Then
  
  $\rsupfun{K} X \sqsupseteq \bigsqcap^{\mathscr{F}}_{L \in S, X
  \in \up x} \rsupfun{L} X$ for every $X \in \up x$
  
  thus by properties of generalized filter bases ($\setcond{ \rsupfun{L}X
  }{ L \in S }$ is a filter base)
  
  $\rsupfun{K} X \sqsupseteq \bigsqcap^{\mathscr{F}}_{X \in
  \up x} \rsupfun{L} X$ for some $L \in S$ and thus
  
  by properties of generalized filter bases ($\setcond{ \rsupfun{L}
  X }{ X \in \up x }$ is a filter base) for some $X' \in \up x$
  
  $\rsupfun{K} X \sqsupseteq \rsupfun{L} X'
  \sqsupseteq \supfun{L} x$
  
  So $\supfun{K} x \sqsupseteq \supfun{L} x$ because this
  equality holds for every $X \in \up x$. Therefore $K \sqsupseteq L$.
\end{proof}

\begin{prop}
$\up f$ is a base of~$f$ for every funcoid~$f$.
\end{prop}

\begin{proof}
Denote $S=\up f$.
$\bigsqcap^{\mathsf{FCD}} S = f$.
If $X \in \up \bigsqcap^{\mathsf{FCD}} S$ then $X \in \up
f$ and then $X \in S$ and thus $S$ is a base of a funcoid because $X
\sqsubseteq X$.
\end{proof}

\begin{example}
A base of a funcoid which is not a filter base.
\end{example}

\begin{proof}
Consider $f=\id^{\mathsf{FCD}}_{\Omega}$. We know that $\up f$ is not a
filter base. But it is a base of a funcoid accordingly the last proposition.
\end{proof}

\begin{prop}
$f = \bigsqcap^{\mathsf{FCD}} S$ for every base~$S$ of a funcoid~$f$.
\end{prop}

\begin{proof}
$f$ is an lower bound of $S$ by definition. So $f \sqsubseteq
\bigsqcap^{\mathsf{FCD}} S$.

We have $\forall X \in \up f \exists T \in S : T \sqsubseteq X$ thus
$\bigsqcap^{\mathsf{FCD}} S \sqsubseteq \bigsqcap^{\mathsf{FCD}}
\up f = f$.

So $f = \bigsqcap^{\mathsf{FCD}} S$.
\end{proof}

\fxnote{What are funcoids~$f$ such that $\up f$ is a filter? or any its base is a filter base?}

\begin{thm}
  Let $S$ be a set of $\mathbf{Rel}$-morphisms. If for every $X, Y
  \in S$ we have $\up (X \sqcap^{\mathsf{FCD}} Y) \subseteq S$
  then there exists a funcoid $f$ such that $S = \up f$.
\end{thm}

\begin{proof}
  \fxwarning{There are errors in this proof. I hope the proof idea is right however.}

  $S$~is an upper set because
  $X\in S\Rightarrow \up (X \sqcap^{\mathsf{FCD}} X) \subseteq S\Rightarrow \up X\subseteq S$.

  Let $S' = S \cap \Gamma$.
  
  Every element of~$S$ is representable as a meet (on the set of $\mathbf{Rel}$-morphisms) of a subset of~$S'$ because
  $T\in S\Rightarrow T=\bigsqcap \up^{\Gamma} T=\bigsqcap(\Gamma\cap\up T)$
  but $\Gamma\cap\up T$ is a subset of~$S'$. \fxnote{Need this paragraph?}
  
  Every element of~$S$ is representable as a meet (on the set of funcoids) of a subset of~$S'$ because
  $T\in S\Rightarrow T=\bigsqcap^{\mathsf{FCD}} \up^{\Gamma} T=\bigsqcap^{\mathsf{FCD}} (\Gamma\cap\up T)$
  but $\Gamma\cap\up T$ is a subset of~$S'$.
  
  Let's prove $\bigsqcap^{\mathsf{FCD}} S' = \bigsqcap^{\mathsf{FCD}} S$.
  
  $\bigsqcap^{\mathsf{FCD}} S' \sqsupseteq \bigsqcap^{\mathsf{FCD}} S$ is obvious.
  
  $\bigsqcap^{\mathsf{FCD}} S = \bigsqcap^{\mathsf{FCD}} \bigsqcap^{\mathsf{FCD}}_{K\in S} T_K \sqsupseteq \bigsqcap^{\mathsf{FCD}} S'$.
  where $T_K\in\subsets S'$.
  So $\bigsqcap^{\mathsf{FCD}} S' = \bigsqcap^{\mathsf{FCD}} S$.
  
  Then  because the filtrator
  $(\mathsf{FCD} ; \Gamma)$ if filtered. $S'$ is a filter on $\Gamma$
  because $\up^{\Gamma} (X \sqcap Y) \subseteq S'$ for every $X, Y \in
  S'$. Take $f = \bigsqcap^{\mathsf{FCD}} S$. We have
  $\up^{\Gamma} f = S'$.
  
  $\up f = \setcond{ \bigsqcap^{\mathsf{FCD}} K }{
  K \in \subsets S' } = S$ because every $T \in
  \up f$ is representable as a meet of elements of $\up^{\Gamma} f$.
  \fxerror{$\bigsqcap^{\mathsf{FCD}} K \rightarrow \bigsqcap K$.}
\end{proof}

\noindent\makebox[\linewidth]{\rule{\paperwidth}{0.4pt}}

Below I present mainly unproved conjectures.

\begin{lem}
$\up (f \sqcap^{\mathsf{FCD}} g) \subseteq \bigcup \setcond{
\up (F \sqcap^{\mathsf{FCD}} G) }{ F \in \up f, G \in \up g }$
or equivalently: If $Z\in\up (f \sqcap^{\mathsf{FCD}} g)$ then there exists
$F \in \up f$, $G \in \up g$ such that $Z\in\up (F \sqcap^{\mathsf{FCD}} G)$.
\fxnote{What's about an infinitary variant of this lemma (and its consequences)?}
\end{lem}

\begin{proof}
??
\end{proof}

\begin{prop}
$\up (f \sqcap^{\mathsf{FCD}} g) \subseteq \setcond{ F \sqcap G }{ F \in \up f, G \in \up g }$.
\end{prop}

\begin{proof}
$\up (F \sqcap^{\mathsf{FCD}} G) \subseteq \up (F
\sqcap G)$. So $\up (f \sqcap^{\mathsf{FCD}} g) \subseteq ?? \subseteq \bigcup
\setcond{ \up (F \sqcap G) }{ F \in \up f,
G \in \up g } \subseteq \setcond{ F \sqcap G }{ F \in \up f, G \in \up g }$ because every
element of $X \in \up (F \sqcap G)$ can be represented as $F' \sqcap G'$
for $F' \in \up f$, $G' \in \up g$ (take $F' = F \sqcup X$ and $G'
= G \sqcup X$; then $F' \sqcap G' = (F \sqcup X) \sqcap (G \sqcup X) = (F
\sqcap G) \sqcup X = X$).
\end{proof}

\begin{cor}
$\up (f_0 \sqcap^{\mathsf{FCD}} \ldots
\sqcap^{\mathsf{FCD}} f_n) \subseteq \setcond{ F_0 \sqcap \ldots \sqcap
F_n }{ F_0 \in \up f_0 \wedge \ldots \wedge F_n \in \up f_n }$.
\end{cor}

\begin{proof}
~
\begin{multline*}
\up (f_0 \sqcap^{\mathsf{FCD}} \ldots
\sqcap^{\mathsf{FCD}} f_{n + 1}) \subseteq \setcond{ F \sqcap G
}{ F \in \up (f_0 \sqcap^{\mathsf{FCD}}
\ldots \sqcap^{\mathsf{FCD}} f_n), G \in \up f_{n + 1} } \subseteq \\
\setcond{ F \sqcap G }{ F \in \setcond{ F_0
\sqcap \ldots \sqcap F_n }{ F_0 \in \up f_0
\wedge \ldots \wedge F_n \in \up f_n }, G \in \up f_{n + 1} } = \\
\setcond{ F_0 \sqcap \ldots \sqcap F_{n + 1} }{
F_0 \in \up f_0 \wedge \ldots \wedge F_{n + 1} \in
\up f_{n + 1} }.
\end{multline*}
\end{proof}

\begin{lem}
  Let for every $X, Y \in S$ and $Z \in \up (X
  \sqcap^{\mathsf{FCD}} Y)$ there is a $T \in S$ such that $T
  \sqsubseteq Z$.
  
  Then for every $X_0, \ldots, X_n \in S$ and $Z \in \up (X_0
  \sqcap^{\mathsf{FCD}} \ldots \sqcap^{\mathsf{FCD}} X_n)$ there
  is a $T \in S$ such that $T \sqsubseteq Z$. [The reverse implication is
  trivial.]
\end{lem}

\begin{proof}
Prove by induction: Let it holds for $n$. Let $Z \in \up (X_0
\sqcap^{\mathsf{FCD}} \ldots \sqcap^{\mathsf{FCD}} X_{n + 1})$.

$Z \in \up (Q \sqcap^{\mathsf{FCD}} X_{n + 1})$ for some $Q \in
\up (X_0 \sqcap^{\mathsf{FCD}} \ldots
\sqcap^{\mathsf{FCD}} X_n)$ (first lemma).

We have $T \in S$ such that $T \sqsubseteq Q$. So $Z \in \up (T
\sqcap^{\mathsf{FCD}} X_{n + 1})$; thus there is $T' \in S$ such that
$T' \sqsubseteq Z$.
\end{proof}

\begin{lem}
  Let for every $X, Y \in S$ and $Z \in \up (X
  \sqcap^{\mathsf{FCD}} Y)$ there is a $T \in S$ such that $T
  \sqsubseteq Z$.
  
  If $Z \in \up \bigsqcap S$ then $Z \in \up (X_0
  \sqcap^{\mathsf{FCD}} \ldots \sqcap^{\mathsf{FCD}} X_n)$ for
  some $n \in \mathbb{N}$, $X_0, \ldots, X_n \in S$.
\end{lem}

\begin{proof}
  ?? Try to prove with transfinite induction or transfinite recursion.

  $\bigsqcap S = \bigsqcap \up \bigsqcap S$ ??
  
  $Z \in \up \bigsqcap \up \bigsqcap S \Leftrightarrow Z \in
  \up \bigsqcap S \Rightarrow ? ?$
\end{proof}

\begin{prop}
  A set $S$ of binary relations [generalize for sets of funcoids] is a base of
  a funcoid iff for every $X, Y \in S$ and $Z \in \up (X
  \sqcap^{\mathsf{FCD}} Y)$ there is a $T \in S$ such that $T
  \sqsubseteq Z$.
  \fxnote{Try to prove by replacing a base of a funcoid~$f$ with~$\up f$. (Proved above.)}
\end{prop}

\begin{proof}
  ~
  \begin{description}
    \item[$\Rightarrow$] Let $S$ be a base of a funcoid $f$. Take $X, Y \in S$
    and $Z \in \up (X \sqcap^{\mathsf{FCD}} Y)$. Thus $Z \in
    \up f$. Thus $\exists T \in S : T \sqsubseteq Z$.
    
    \item[$\Leftarrow$] Take $f = \bigsqcap^{\mathsf{FCD}} S$. Because
    the filtrators of funcoids is filtered, $f =
    \bigsqcap^{\mathsf{FCD}} \up f$.
    
    ?? For every $X \in S$ [need $X \in \up f$ instead] and $Z \in
    \up X$ there is $T \in S$ such that $T \sqsubseteq Z$. Take $Z = X$
    and we have $T \sqsubseteq X$.
    
    ??
  \end{description}
\end{proof}

\fxnote{It seems that the above (when complete) can be easily generalized for more general filtrators.}

Note that the initial incentive to consider all above was to prove this conjecture~\bookref{fcd-comp-ent}.